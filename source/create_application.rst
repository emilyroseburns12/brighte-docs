Restful API
###########

Applications
~~~~~~~~~~~~

Create Consumer Application
---------------------------

In order to create a new consumer application you need to supply all the data as below. For more information about the API please see the `documentation <restful.html#restful-api>`_.

Endpoint URL:

+-------------+--------------------------------------------------+
| Method Type | Url                                              |
+=============+==================================================+
| POST        | portal.brighte.com.au/rest/v1/vendor/application |
+-------------+--------------------------------------------------+

Parameters:

+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| Name                                 | Value   | Description                                                                                                                                 |
+======================================+=========+=============================================================================================================================================+
| account_id                           | string  | account to add the application for                                                                                                          |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| joint_application                    | boolean | default false, is this a joint application                                                                                                  |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| applicant_title                      | string  | for example, "MR", "MRS", etc. title of applicant                                                                                           |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| applicant_first_name                 | string  | first name of applicant                                                                                                                     |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *applicant_middle_name*              | string  | *(optional)* middle name of applicant                                                                                                       |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| applicant_last_name                  | string  | last name of applicant                                                                                                                      |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| applicant_marital_status             | string  | can only be "Defacto", "Married" or "Single"                                                                                                |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| applicant_dependants                 | string  | can only be "0", "1", "2" or "3+"                                                                                                           |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| applicant_gender                     | string  | can either be "Male" or "Female"                                                                                                            |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| applicant_dob                        | date    | date of birth of applicant, formatting like "1986-01-01"                                                                                    |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| applicant_email                      | string  | applicant's email, needs to be unique                                                                                                       |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| applicant_mobile                     | string  | applicant's mobile number, needs to be numbers                                                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| privacy_statement                    | string  | version of the statement, can get by hitting api.brighte.com.au/content/{contentType}, latest version is "2017-12-05"                       |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| privacy_consent                      | boolean | privacy consent, applicant must agree to privacy policy                                                                                     |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| marketing_consent                    | boolean | whether you like to accept marketing material, this field can not be empty                                                                  |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *mailing_address_unit_number*        | string  | *(optional)* unit number of mailing address                                                                                                 |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| mailing_address_street_number        | string  | street number of mailing address                                                                                                            |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| mailing_address_street_name          | string  | street name of mailing address                                                                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| mailing_address_suburb               | string  | suburb name of mailing address                                                                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| mailing_address_state_code           | string  | state name of mailing address                                                                                                               |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| mailing_address_postcode             | string  | postcode of mailing address                                                                                                                 |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| identification                       | string  | type of identification, can only be "passport", "license" (driver license) or "medicare" (medicare card)                                    |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *passport_country*                   | string  | *(optional)* if identification field is "passport" then this field is required                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *passport_number*                    | string  | *(optional)* if identification field is "passport" then this field is required                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *medicare_number*                    | string  | *(optional)* if identification field is "medicare" then this field is required                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *medicare_reference*                 | string  | *(optional)* if identification field is "medicare" then this field is required                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *medicare_colour*                    | string  | *(optional)* if identification field is "medicare" then this field is required                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *medicare_expiry*                    | date    | *(optional)* expiry date of the medicare card, if identification field is "medicare" then this field is required                            |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *license_number*                     | string  | *(optional)* if identification field is "license" then this field is required                                                               |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *license_state_code*                 | string  | *(optional)* if identification field is "license" then this field is required                                                               |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *license_expiry*                     | date    | *(optional)* expiry date of the driver license, if identification field is "license" then this field is required                            |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| employed_more_than_30_hours          | boolean | if the applicant work on a full time basis                                                                                                  |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *employer_name*                      | string  | *(optional)* employer's name, if employed_more_than_30_hours field is true then this field is required                                      |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *employer_suburb*                    | string  | *(optional)* employer's suburb, if employed_more_than_30_hours field is true then this field is required                                    |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *employer_phone*                     | string  | *(optional)* employer's phone number , if employed_more_than_30_hours field is true then this field is required                             |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *employer_hours_worked*              | integer | *(optional)* how many hours applicant work, if employed_more_than_30_hours field is true then this field is required                        |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| pensioner_or_veteran                 | boolean | if the applicant is a pensioner or veteran                                                                                                  |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *pensioner_or_veteran_type*          | string  | *(optional)* can only be "Pensioner", "Veteran" or "Self-Funded Retiree", if pensioner_or_veteran field is true then this field is required |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| self_employed                        | boolean | if the applicant is self-employed                                                                                                           |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *business_trading_name*              | string  | *(optional)* trading name, if self_employed field is true then this field is required                                                       |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *business_abn*                       | string  | *(optional)* abn number, if self_employed field is true then this field is required                                                         |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *business_phone_number*              | string  | *(optional)* business phone number, if self_employed field is true then this field is required                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| base_net_income_amount               | integer | main applicant's income amount                                                                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| base_net_income_frequency            | string  | main applicant's income frequency, can only be "weekly", "fortnightly" or "monthly"                                                         |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *partners_base_net_income_amount*    | integer | *(optional)* partner's income amount                                                                                                        |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *partners_base_net_income_frequency* | string  | *(optional)* partner's income frequency, can only be "weekly", "fortnightly" or "monthly"                                                   |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| other_taxable_net_income_amount      | integer | other taxable income amount                                                                                                                 |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| other_taxable_net_income_frequency   | string  | other taxable income frequency, can only be "weekly", "fortnightly" or "monthly"                                                            |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| mortgage_repayment_amount            | integer | repayment amount                                                                                                                            |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| mortgage_repayment_frequency         | string  | repayment frequency, can only be "weekly", "fortnightly" or "monthly"                                                                       |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| other_loan_commitments_amount        | integer | other loan commitments amount                                                                                                               |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| other_loan_commitments_frequency     | string  | *(optional)* other loan commitments frequency, can only be "weekly", "fortnightly" or "monthly"                                             |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| household_credit_card_limit_amount   | integer | credit card limit                                                                                                                           |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| household_living_costs_amount        | integer | household living costs amount                                                                                                               |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| household_living_costs_frequency     | string  | household living costs frequency, can only be "weekly", "fortnightly" or "monthly"                                                          |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| fill_property_from_mailing_address   | string  | *(optional)* "primary" means the applicant owned a property whose address is the same as mailing address                                    |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *property_unit_number*               | string  | *(optional)* unit number of the property                                                                                                    |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *property_street_number*             | string  | *(optional)* street number of the property, if fill_property_from_mailing_address field is not "primary" then this field is required        |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *property_street_name*               | string  | *(optional)* street name of the property, if fill_property_from_mailing_address field is not "primary" then this field is required          |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *property_suburb*                    | string  | *(optional)* suburb of the property, if fill_property_from_mailing_address field is not "primary" then this field is required               |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *property_state_code*                | string  | *(optional)* state code of the property, if fill_property_from_mailing_address field is not "primary" then this field is required           |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *property_postcode*                  | string  | *(optional)* postcode of the property, if fill_property_from_mailing_address field is not "primary" then this field is required             |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| payment_method                       | string  | "bankaccount", cannot be empty                                                                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *direct_debit_account_name*          | string  | *(optional)* if payment_method field is "bankaccount" then this field is required                                                           |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *direct_debit_bsb*                   | string  | *(optional)* if payment_method field is "bankaccount" then this field is required                                                           |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *direct_debit_account_number*        | string  | *(optional)* if payment_method field is "bankaccount" then this field is required                                                           |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *cardholder_name*                    | string  | *(optional)* if payment_method field is "creditcard" then this field is required                                                            |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *card_number*                        | string  | *(optional)* if payment_method field is "creditcard" then this field is required                                                            |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *expiry_date_month*                  | string  | *(optional)* if payment_method field is "creditcard" then this field is required                                                            |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| *expiry_date_year*                   | string  | *(optional)* if payment_method field is "creditcard" then this field is required                                                            |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| deposit_amount                       | integer | deposit amount                                                                                                                              |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| repayment_term                       | integer | repayment term in month                                                                                                                     |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| total_purchase_amount                | integer | total purchase amount                                                                                                                       |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| product_category                     | string  | "Solar Panel Package"                                                                                                                       |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| product_description                  | string  | product description                                                                                                                         |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| agreement_consent                    | boolean | agreement consent, applicant must agree to Brighte policy                                                                                   |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| agreement_statement                  | string  | version of the statement, can get by hitting api.brighte.com.au/content/{contentType}, latest version is "2018-02-20"                       |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| direct_debit_agreement               | boolean | direct debit consent, applicant must agree to direct debit agreement                                                                        |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| direct_debit_statement               | string  | version of the statement, can get by hitting api.brighte.com.au/content/{contentType}, latest version is "2018-02-20"                       |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+
| security_code                        | string  | security code received from sms, this code will be send to assigned mobile by hit api.brighte.com.au/rest/v1/vendor/application/secure-code |
+--------------------------------------+---------+---------------------------------------------------------------------------------------------------------------------------------------------+

Sample request:

HTTP

.. code-block:: http

    POST /rest/v1/vendor/application HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

    {
        "joint_application": false,
        "applicant_title":"Mr",
        "applicant_first_name":"testfive",
        "applicant_middle_name":"",
        "applicant_last_name":"tester",
        "applicant_marital_status":"Married",
        "applicant_dependants":"1",
        "applicant_gender":"Male",
        "applicant_dob": "1986-09-03",
        "applicant_email":"single.applicantdhdsdshd33@test.com",
        "applicant_mobile":"04561233213",
        "privacy_statement":"",
        "privacy_consent": false,
        "marketing_consent": false,
        "mailing_address_unit_number":"",
        "mailing_address_street_number":"",
        "mailing_address_street_name":"Princes Highway",
        "mailing_address_suburb":"Kirrawee",
        "mailing_address_state_code":"NSW",
        "mailing_address_postcode":"2232",
        "identification":"license",
        "passport_country":"Australia",
        "passport_number":"123123123123",
        "employer_hours_worked":10,
        "base_net_income_amount":2000,
        "base_net_income_frequency":"Weekly",
        "partners_base_net_income_amount":981,
        "partners_base_net_income_frequency":"Weekly",
        "other_taxable_net_income_amount":1,
        "other_taxable_net_income_frequency":"Weekly",
        "mortgage_repayment_amount":1,
        "mortgage_repayment_frequency":"Weekly",
        "other_loan_commitments_amount":1,
        "other_loan_commitments_frequency":"Weekly",
        "household_credit_card_limit_amount":1,
        "household_living_costs_amount":1,
        "household_living_costs_frequency":"Weekly",
        "fill_property_from_mailing_address":"dsadasd",
        "property_unit_number":"",
        "property_street_number":"",
        "property_street_name":"Princes Highway",
        "property_suburb":"Kirrawee",
        "property_state_code":"NSW",
        "property_postcode":"2232",
        "payment_method":"bankaccount",
        "direct_debit_account_name":"daodpnd",
        "direct_debit_bsb":"123456",
        "direct_debit_account_number":"123123123123123",
        "employed_more_than_30_hours": true,
        "pensioner_or_veteran":false,
        "self_employed":true,
        "deposit_amount":2000,
        "repayment_term":12,
        "total_purchase_amount":12000,
        "product_category":"Solar Panel Package",
        "product_description":"",
        "agreement_consent":true,
        "direct_debit_agreement":true,
        "direct_debit_statement":"2018-02-20",
        "agreement_statement": "2018-02-20",
        "security_code":"111111",
        "pensioner_or_veteran_type":"pensioners"
    }

Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "application": "Application POD"
    }

Resources:

* `Application POD <dataTypes.html#application>`_


Error codes:

+------+--------+------------------------+--------------------------------+
| Code | Status | Message                | Reason                         |
+======+========+========================+================================+
| 401  | 4010   | Unauthorized user.     | Authentication failed.         |
+------+--------+------------------------+--------------------------------+
| 401  | 4010   | Not authorized.        | Invalid account id.            |
+------+--------+------------------------+--------------------------------+
| 400  | 4402   | Invalid data provided. | See `details` in the response. |
+------+--------+------------------------+--------------------------------+
