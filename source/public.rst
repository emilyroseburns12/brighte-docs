Public API
###########

Initial Setup for the Rest API
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Brighte exposes some of the tools like calculators, product offerings etc.


Content Type
------------

All endpoints should be able to accept json so you will need to add the following headers:

+--------------+------------------+
| Key          | Value            |
+==============+==================+
| Accept       | application/json |
+--------------+------------------+
| Content-Type | application/json |
+--------------+------------------+

Date and time formats
---------------------

The rest API uses the **UTC timezone** for both input and output parameter. The timestamps returned from this API use **ISO8601** format (e.g. `2016-08-08T06:54:36+00:00`) which includes the timezone as well.


------------------------------------------------------------------------------------------------------------------------

Calculators
~~~~~~~~~~~

Finance
--------------

Calculate financing. Repayments, installment, fees.


Endpoint URL:

+-------------+-------------------------------------------------------+
| Method Type | Url                                                   |
+=============+=======================================================+
| POST        | portal.brighte.com.au/rest/pub/v1/calculators/finance |
+-------------+-------------------------------------------------------+


Parameters:

+-----------------------+----------+----------------------------------------------------------------------------------------+
| Name                  | Value    | Description                                                                            |
+=======================+==========+========================================================================================+
| totalPurchaseAmount   | currency | How much do you want to borrow.                                                        |
+-----------------------+----------+----------------------------------------------------------------------------------------+
| repaymentTerm         | string   | The repayment term in months.                                                          |
+-----------------------+----------+----------------------------------------------------------------------------------------+
| *depositAmount*       | currency | *(optional, default 0)* How much ist he deposit for the loan.                          |
+-----------------------+----------+----------------------------------------------------------------------------------------+
| *addEstablishmentFee* | bool     | *(optional, default true)* If this is a first time customer add the establishment fee. |
+-----------------------+----------+----------------------------------------------------------------------------------------+
| *financeType*         | string   | *(optional, default CONSUMER)* Type of the application (CONSUMER or SME)               |
+-----------------------+----------+----------------------------------------------------------------------------------------+


Sample request:

HTTP

.. code-block:: http

    POST /rest/pub/v1/calculators/finance HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    Cache-Control: no-cache

    {
        "totalPurchaseAmount": 10000,
        "repaymentTerm": 24,
        "financeType": "consumer",
        "depositAmount": 1500,
        "addEstablishmentFee": 0
    }

Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "data": {
            "repayment_amount": 165.47,
            "instalments": 52,
            "total_purchase_amount": 10000,
            "deposit_amount": 1500,
            "financed_amount": 8500,
            "establishment_fee": 0,
            "weekly_account_keeping_fee": 1,
            "annual_fee": 0,
            "late_payment_fee": 4.99
        }
    }

Error codes:

+------+--------+-----------------+-------------------------------------+
| Code | Status | Message         | Reason                              |
+======+========+=================+=====================================+
| 400  | 4402   | Invalid input.  | Wrong format or persence of inputs. |
+------+--------+-----------------+-------------------------------------+
| 500  | 5000   | Internal error. | Contact support.                    |
+------+--------+-----------------+-------------------------------------+
