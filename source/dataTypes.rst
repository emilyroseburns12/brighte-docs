**************
API Data Types
**************

Here is a complete list of data types used by Brighte API.


Simple Data Types
=================


+------------+-----------------------------------------------------------------------------------------------------+
| Data Type  | Description                                                                                         |
+============+=====================================================================================================+
| timestamp  | String representation of date and time value, up to seconds, in ISO8601                             |
|            | e.g. '2012-10-01T00:00:00+10:00'                                                                    |
+------------+-----------------------------------------------------------------------------------------------------+
| date       | String representation of date value, in YYYY-MM-DD format.                                          |
|            | e.g. ‘2012-10-01’                                                                                   |
+------------+-----------------------------------------------------------------------------------------------------+
| currency   | 64-bit signed integer in cents (i.e. range from -9223 trillion to 9223 trillion).                   |
|            | A leading minus sign indicates a negative number. No leading plus sign is used for positive number. |
|            | e.g. $238.50 will be                                                                                |
|            | presented as 23850 -$37.95                                                                          |
|            | will be presented as -3795                                                                          |
+------------+-----------------------------------------------------------------------------------------------------+
| percentage | 32-bit signed integer (i.e. range from -2147 million to 2147 million).                              |
|            | Assumption: no more than 2 decimal places is used.                                                  |
|            | e.g. 5.95% is presented as 595.                                                                     |
|            | -2% is presented as -200.                                                                           |
|            | 0.89% is presented as 89.                                                                           |
+------------+-----------------------------------------------------------------------------------------------------+
| string     | UTF-8 encoded string.                                                                               |
+------------+-----------------------------------------------------------------------------------------------------+
| file       | String representation of file (base64 encoded).                                                     |
+------------+-----------------------------------------------------------------------------------------------------+
| integer    | 64-bit signed integer (i.e. range from -9223 trillion to 9223 trillion).                            |
|            | A leading minus sign indicates a negative number. No leading plus sign is used for positive number. |
+------------+-----------------------------------------------------------------------------------------------------+
| boolean    | integer 1 or integer 0                                                                              |
+------------+-----------------------------------------------------------------------------------------------------+



Enums
=====


+-------------------------+-----------------------------+
| Enum                    | Possible Values             |
+=========================+=============================+
| applicationStatus       | APPROVED                    |
|                         | C.APPROV                    |
|                         | DECLINED                    |
|                         | PAID                        |
|                         | QUOTE                       |
|                         | REFER                       |
|                         | WITHDRAW                    |
+-------------------------+-----------------------------+
| applicationDocumentType | INVOICE                     |
|                         | STC                         |
|                         | CCA                         |
+-------------------------+-----------------------------+
| calloutStatus           | OPEN                        |
|                         | FULLFILLED                  |
|                         | CLOSED                      |
+-------------------------+-----------------------------+
| contentType             | DIRECT_DEBIT_AGREEMENT      |
|                         | PRIVACY_POLICY              |
|                         | TNC                         |
+-------------------------+-----------------------------+
| gender                  | FEMALE                      |
|                         | MALE                        |
+-------------------------+-----------------------------+
| identificationType      | DRIVER_LICENCE              |
|                         | MEDICARE_CARD               |
|                         | PASSPORT                    |
+-------------------------+-----------------------------+
| maritalStatus           | DEFACTO                     |
|                         | MARRIED                     |
|                         | SINGLE                      |
+-------------------------+-----------------------------+
| medicareColour          | BLUE                        |
|                         | GREEN                       |
|                         | YELLOW                      |
+-------------------------+-----------------------------+
| paymentMethod           | BANK_ACCOUNT                |
|                         | VISA                        |
|                         | MASTERCARD                  |
+-------------------------+-----------------------------+
| pensionerOrVeteranType  | AGED_PENSION                |
|                         | RETURNED_SERVICEMEN_PENSION |
|                         | SELF_FUNDED_RETIREE         |
+-------------------------+-----------------------------+
| personTitle             | DR                          |
|                         | MISS                        |
|                         | MR                          |
|                         | MRS                         |
|                         | MS                          |
+-------------------------+-----------------------------+
| quoteStatus             | COMPLETED                   |
|                         | SENT                        |
+-------------------------+-----------------------------+
| stateCode               | ACT                         |
|                         | NSW                         |
|                         | NT                          |
|                         | QLD                         |
|                         | SA                          |
|                         | TAS                         |
|                         | VIC                         |
|                         | WA                          |
+-------------------------+-----------------------------+
| userRole                | ADMINISTRATOR               |
|                         | TEAM MEMBER                 |
+-------------------------+-----------------------------+


Complex Data Types (aka POD objects)
====================================


Complex data types are implemented as POD objects.

Application
-----------

JSON template for **Application** POD:

.. code-block:: json

    {
        "id": string,
        "applicants": Applicant POD[],
        "team_member": string,
        "financed_amount": currency,
        "repayment_term": integer,
        "product_category": string,
        "product_description": string,
        "status": enum<applicationStatus>,
        "created": timestamp
    }

Resources:

* `Applicant POD <dataTypes.html#applicant>`_

+---------------------+-----------------------------------------------+-----------------------------------------------------------------------+
| Name                | Value                                         | Description                                                           |
+=====================+===============================================+=======================================================================+
| id                  | string                                        | This is the unique id.                                                |
+---------------------+-----------------------------------------------+-----------------------------------------------------------------------+
| applicants          | `Applicant POD[] <dataTypes.html#applicant>`_ | Primary and joint applicants.                                         |
+---------------------+-----------------------------------------------+-----------------------------------------------------------------------+
| team_member         | string                                        | Name of the agent submitted the application: <first_name> <last_name> |
+---------------------+-----------------------------------------------+-----------------------------------------------------------------------+
| financed_amount     | currency                                      | How much to finance with this application.                            |
+---------------------+-----------------------------------------------+-----------------------------------------------------------------------+
| repayment_term      | integer                                       | In months.                                                            |
+---------------------+-----------------------------------------------+-----------------------------------------------------------------------+
| product_category    | string                                        | Category  of the product in this application.                         |
+---------------------+-----------------------------------------------+-----------------------------------------------------------------------+
| product_description | string                                        | Description of the financed items.                                    |
+---------------------+-----------------------------------------------+-----------------------------------------------------------------------+
| status              | enum                                          | See applicationStatus.                                                |
+---------------------+-----------------------------------------------+-----------------------------------------------------------------------+
| created             | timestamp                                     | Time of creation.                                                     |
+---------------------+-----------------------------------------------+-----------------------------------------------------------------------+

Applicant
-----------

JSON template for **Applicant** POD:

.. code-block:: json

    {
        "id": string,
        "name": string,
        "email": string,
        "mobile": string,
        "dob": date,
        "joint_applicant": boolean
    }

+-----------------+---------+--------------------------------------------------------------------------+
| Name            | Value   | Description                                                              |
+=================+=========+==========================================================================+
| id              | string  | This is the unique id.                                                   |
+-----------------+---------+--------------------------------------------------------------------------+
| name            | string  | Full name of the applicant.                                              |
+-----------------+---------+--------------------------------------------------------------------------+
| email           | string  | Email of the applicant.                                                  |
+-----------------+---------+--------------------------------------------------------------------------+
| mobile          | string  | Mobile phone number of the applicant.                                    |
+-----------------+---------+--------------------------------------------------------------------------+
| dob             | date    | Date of birth of the applicant.                                          |
+-----------------+---------+--------------------------------------------------------------------------+
| joint_applicant | boolean | Flag indicating if this is the joint applicant or the primary applicant. |
+-----------------+---------+--------------------------------------------------------------------------+

ApplicationDocument
-------------------

JSON template for **ApplicationDocument** POD:

.. code-block:: json

    {
        "type": enum<applicationDocumentType>,
        "filename": string,
    }

+----------+--------+------------------------------+
| Name     | Value  | Description                  |
+==========+========+==============================+
| type     | enum   | See applicationDocumentType. |
+----------+--------+------------------------------+
| filename | string | Name of the file.            |
+----------+--------+------------------------------+


Quote
-----

JSON template for **Quote** POD:

.. code-block:: json

     {
        "id": string,
        "applicant_email": string,
        "total_purchase_amount": currency,
        "deposit_amount": currency,
        "repayment_term": integer,
        "product_category": string,
        "product_description": string,
        "created": timestamp,
        "team_member": string,
        "applicant": string,
        "status": enum<quoteStatus>
    }

+-----------------------+-----------+-----------------------------------------------------------------------+
| Name                  | Value     | Description                                                           |
+=======================+===========+=======================================================================+
| applicant_email       | string    | Email of the applicant and the target where the quote is to be sent.  |
+-----------------------+-----------+-----------------------------------------------------------------------+
| total_purchase_amount | currency  | Proposed financed amount.                                             |
+-----------------------+-----------+-----------------------------------------------------------------------+
| deposit_amount        | currency  | Proposed deposit amount.                                              |
+-----------------------+-----------+-----------------------------------------------------------------------+
| repayment_term        | integer   | Repayment term in months.                                             |
+-----------------------+-----------+-----------------------------------------------------------------------+
| product_category      | string    | Category  of the product in this application.                         |
+-----------------------+-----------+-----------------------------------------------------------------------+
| product_description   | string    | Description of the financed items.                                    |
+-----------------------+-----------+-----------------------------------------------------------------------+
| created               | timestamp | Time the quote was created.                                           |
+-----------------------+-----------+-----------------------------------------------------------------------+
| id                    | string    | Unique ID.                                                            |
+-----------------------+-----------+-----------------------------------------------------------------------+
| team_member           | string    | Name of the agent submitted the application: <first_name> <last_name> |
+-----------------------+-----------+-----------------------------------------------------------------------+
| applicant             | string    | Name of applicant: <first_name> <last_name>                           |
+-----------------------+-----------+-----------------------------------------------------------------------+
| status                | enum      | See quoteStatus.                                                      |
+-----------------------+-----------+-----------------------------------------------------------------------+



User
----

JSON template for **User** POD:

.. code-block:: json

    {
        "id": string,
        "role": enum<userRole>,
        "firstName": string,
        "lastName": string,
        "email": string,
        "mobile": string
    }

+------------+--------+-------------------------------------------+
| Name       | Value  | Description                               |
+============+========+===========================================+
| id         | string | Unique ID.                                |
+------------+--------+-------------------------------------------+
| role       | enum   | See userRole.                             |
+------------+--------+-------------------------------------------+
| first_name | string | Users first name.                         |
+------------+--------+-------------------------------------------+
| last_name  | string | Users last name.                          |
+------------+--------+-------------------------------------------+
| email      | string | Unique email of the user.                 |
+------------+--------+-------------------------------------------+
| mobile     | string | Mobile phone number without country code. |
+------------+--------+-------------------------------------------+


Account
-------

JSON template for **Account** POD:

.. code-block:: json

    {
        "id": integer,
        "entity_legal_name": string,
        "entity_trading_name": string,
    }

+---------------------+--------+---------------+
| Name                | Value  | Description   |
+=====================+========+===============+
| id                  | string | Unique ID.    |
+---------------------+--------+---------------+
| entity_legal_name   | string | Trading name. |
+---------------------+--------+---------------+
| entity_trading_name | string | Legal name.   |
+---------------------+--------+---------------+



Content
-------

JSON template for **Content** POD:

.. code-block:: json

    {
        "content_type": enum<contentType>,
        "version": timestamp,
        "content": string,
    }

+--------------+-------------------+---------------+
| Name         | Value             | Description   |
+==============+===================+===============+
| content_type | enum<contentType> | Unique ID.    |
+--------------+-------------------+---------------+
| version      | timestamp         | Trading name. |
+--------------+-------------------+---------------+
| content      | string            | Legal name.   |
+--------------+-------------------+---------------+



ProductCategory
---------------

JSON template for **ProductCategory** POD:

.. code-block:: json

    {
        "name": string,
        "slug": string,
        "icon": string
    }

+------+--------+------------------------------------------------+
| Name | Value  | Description                                    |
+======+========+================================================+
| name | string | Legal name.                                    |
+------+--------+------------------------------------------------+
| slug | string | Slugged representation of the name (url safe). |
+------+--------+------------------------------------------------+
| icon | string | Product icon url.                              |
+------+--------+------------------------------------------------+


RepaymentAmount
---------------

JSON template for **ProductCategory** POD:

.. code-block:: json

    {
        "repaymentAmount": currency,
        "instalments": integer
    }

+-----------------+----------+-------------------------------------------------------+
| Name            | Value    | Description                                           |
+=================+==========+=======================================================+
| repaymentAmount | currency | The amount of each instalment.                        |
+-----------------+----------+-------------------------------------------------------+
| instalments     | integer  | How many instalments over the desired repayment term. |
+-----------------+----------+-------------------------------------------------------+



Callout
-------

JSON template for **Callout** POD:

.. code-block:: json

    {
        "id": string,
        "first_name": string,
        "last_name": string,
        "phone": string,
        "email": string,
        "total_purchase_amount": currency,
        "deposit_amount": currency,
        "repayment_term": integer,
        "product_category": string,
        "product_description": string,
        "call_datetime": timestamp,
        "status": enum<calloutStatus>,
        "created": timestamp,
        "modified": timestamp,
        "full_name": string,
        "financed_amount": currency
    }

+-----------------------+-----------+----------------------------------------------------------------------+
| Name                  | Value     | Description                                                          |
+=======================+===========+======================================================================+
| id                    | string    | Unique ID.                                                           |
+-----------------------+-----------+----------------------------------------------------------------------+
| first_name            | string    | First name of the client.                                            |
+-----------------------+-----------+----------------------------------------------------------------------+
| last_name             | string    | Last name of the client.                                             |
+-----------------------+-----------+----------------------------------------------------------------------+
| phone                 | string    | Phone number of the client.                                          |
+-----------------------+-----------+----------------------------------------------------------------------+
| email                 | string    | Email of the client.                                                 |
+-----------------------+-----------+----------------------------------------------------------------------+
| total_purchase_amount | currency  | Proposed financed amount.                                            |
+-----------------------+-----------+----------------------------------------------------------------------+
| deposit_amount        | currency  | Proposed deposit amount.                                             |
+-----------------------+-----------+----------------------------------------------------------------------+
| repayment_term        | integer   | Repayment term in months.                                            |
+-----------------------+-----------+----------------------------------------------------------------------+
| product_category      | string    | Category  of the product in this application.                        |
+-----------------------+-----------+----------------------------------------------------------------------+
| product_description   | string    | Description of the financed items.                                   |
+-----------------------+-----------+----------------------------------------------------------------------+
| call_datetime         | timestamp | Time of the call.                                                    |
+-----------------------+-----------+----------------------------------------------------------------------+
| status                | enum      | See calloutStatus.                                                   |
+-----------------------+-----------+----------------------------------------------------------------------+
| created               | timestamp | Time the quote was created.                                          |
+-----------------------+-----------+----------------------------------------------------------------------+
| modified              | timestamp | Time the quote was created.                                          |
+-----------------------+-----------+----------------------------------------------------------------------+
| applicant_email       | string    | Email of the applicant and the target where the quote is to be sent. |
+-----------------------+-----------+----------------------------------------------------------------------+
| full_name             | string    | Full name of the client: <first_name> <last_name>                    |
+-----------------------+-----------+----------------------------------------------------------------------+
| financed_amount       | currency  | Proposed financed amount.                                            |
+-----------------------+-----------+----------------------------------------------------------------------+

UserKey
---------------

JSON template for **UserKey** POD:

.. code-block:: json

    {
        "apiId": string,
        "apiSecret": string
    }

+-----------+--------+-------------+
| Name      | Value  | Description |
+===========+========+=============+
| apiId     | string | Api ID.     |
+-----------+--------+-------------+
| apiSecret | string | Api Secret. |
+-----------+--------+-------------+
