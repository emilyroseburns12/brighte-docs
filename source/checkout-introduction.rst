Introduction
============

Brighte allows vendors to create loan applications for their customers. Using the Checkout API, loan applications can be initialised from a vendor web application and completed by the customer on Brighte.

The Checkout solution allows the vendor to direct the customer to Brighte with an API request. The API request from the vendor contains the financing amount and product description. The customer completes a simple and fast loan application process. The customer is redirected back to the vendor web application with an API response containing confirmation of the application.
