Sandbox
#######

Brighte provides a sandbox environment to assist you with integration. Request your sandbox by contacting `Brighte Support`_.

.. _Brighte Support: support@brighte.com.au


Simulated behaviour
~~~~~~~~~~~~~~~~~~~

Status changes are simulated for all applications submitted to the sandbox. All submitted applications will have their statuses changed according to a fixed pattern.

If the last digit of the financed amount (purchase total minus deposit amount) is:

- ``1``, the status is changed to ``C.APPROV`` immediately, then to ``APPROVED`` after several minutes.
- ``2``, the status is changed to ``C.APPROV`` immediately, then to ``DECLINED`` after several minutes.
- ``3``, the status is changed to ``REFER`` immediately, then to ``APPROVED`` after several minutes.
- ``4``, the status is changed to ``REFER`` immediately, then to ``DECLINED`` after several minutes.
- ``5``, the status is changed to ``DECLINED`` immediately.

.. container:: ptable

 ================= ==========================================================
 Example           Simulated behaviour
 ================= ==========================================================
 $1001.00          ``C.APPROV`` immediately, then to ``APPROVED``
 $1005.99          ``DECLINED`` immediately
 ================= ==========================================================
