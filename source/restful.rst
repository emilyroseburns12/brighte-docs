Restful API
###########

Initial Setup for the Rest API
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Brighte exposes some of the functionality required to create and manage applications, quotes and users to authorised users, via a Rest API.

Session keys
------------

You need to acquire access keys in order to interact with the API. Please contact our `support team <mailto:support@brighte.com.au>`_  that will happily assist you in procurring them.

After you have acquired your access keys, you will need to create a session. You will need to use the `create session <restful.html#create-session>`_ endpoint.

Every request will need to include the `X-Session-Key` and `X-Session-Token` headers.

+-----------------+--------+
| Key             | Value  |
+=================+========+
| X-Session-Key   | string |
+-----------------+--------+
| X-Session-Token | string |
+-----------------+--------+

Content Type
------------

All endpoints should be able to accept json so you will need to add the following headers:

+--------------+------------------+
| Key          | Value            |
+==============+==================+
| Accept       | application/json |
+--------------+------------------+
| Content-Type | application/json |
+--------------+------------------+

Date and time formats
---------------------

The rest API uses the **UTC timezone** for both input and output parameter. The timestamps returned from this API use **ISO8601** format (e.g. `2016-08-08T06:54:36+00:00`) which includes the timezone as well.


Account Specifier
-----------------

Most of the entities within the Brighte platform are associated with accounts (branches). Whenever you are trying to interact with these entities, you will need to specify an **account id** within the request. This documentation will tell you where to specify it and how.

------------------------------------------------------------------------------------------------------------------------

Sessions
~~~~~~~~

Create Session
--------------

First call needs to be to create a session by authorizing your user using the api key and the api secret.

*Refer to the top of this guide to find out hot to procure them.*


Endpoint URL:

+-------------+----------------------------------------------+
| Method Type | Url                                          |
+=============+==============================================+
| POST        | portal.brighte.com.au/rest/v1/vendor/session |
+-------------+----------------------------------------------+


Parameters:

+-----------+--------+------------------------+
| Name      | Value  | Description            |
+===========+========+========================+
| apiId     | string | This is the API KEY    |
+-----------+--------+------------------------+
| apiSecret | string | This is the API SECRET |
+-----------+--------+------------------------+


Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/session HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    Cache-Control: no-cache

    {
        "apiId": "xxx",
        "apiSecret": "xxx"
    }


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "sessionKey": "19469f463f1024d4f44c2441bd7d9f13a913b9d6",
        "sessionToken": "109e67ea15892d29ae5ecc241c3f2c399e293004"
    }

Error codes:

+------+--------+----------------------+---------------------------------------+
| Code | Status | Message              | Reason                                |
+======+========+======================+=======================================+
| 401  | 4010   | Invalid credentials. | Wrong Key / Secret combination.       |
+------+--------+----------------------+---------------------------------------+
| 401  | 4010   | Unauthorized.        | User does not have access to the API. |
+------+--------+----------------------+---------------------------------------+


------------------------------------------------------------------------------------------------------------------------

Delete Session
--------------

Delete your session / log out the current user.


Endpoint URL:

+-------------+----------------------------------------------+
| Method Type | Url                                          |
+=============+==============================================+
| DELETE      | portal.brighte.com.au/rest/v1/vendor/session |
+-------------+----------------------------------------------+


Parameters:

*No parameters*

Sample request:

HTTP

.. code-block:: http

    DELETE /rest/vendor/v1/session HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache



Succesfull response:

.. code-block:: json

    {
        "status": 2000
    }

Error codes:

+------+--------+---------------------------+---------------------------------------+
| Code | Status | Message                   | Reason                                |
+======+========+===========================+=======================================+
| 401  | 4010   | Unauthorized.             | User does not have access to the API. |
+------+--------+---------------------------+---------------------------------------+
| 400  | 4402   | Invalid session.          | Session not valid for deletion.       |
+------+--------+---------------------------+---------------------------------------+
| 400  | 4010   | Failed to delete session. | Session not found.                    |
+------+--------+---------------------------+---------------------------------------+

------------------------------------------------------------------------------------------------------------------------

Accounts
~~~~~~~~

Get Accounts
------------

Get list of all the accounts associated with this user.


Endpoint URL:

+-------------+-----------------------------------------------+
| Method Type | Url                                           |
+=============+===============================================+
| GET         | portal.brighte.com.au/rest/v1/vendor/accounts |
+-------------+-----------------------------------------------+


Parameters:

*No Parameters*


Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/account HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "accounts": [
            Account POD
        ],
    }

Resources:

* `Account POD <dataTypes.html#account>`_

Error codes:

+------+--------+----------------------+---------------------------------------------------+
| Code | Status | Message              | Reason                                            |
+======+========+======================+===================================================+
| 404  | 4041   | No account found.    | No account set for current user, contact support. |
+------+--------+----------------------+---------------------------------------------------+

------------------------------------------------------------------------------------------------------------------------

Applications
~~~~~~~~~~~~

Get Applications
----------------

Return all the applications.

The results are ordered by creation date. There is a maximum limit of 500 records per call and the next result set can be retrieved by utilizing the `pageNumber` parameter and the `hasMoreRecords` return field.

Endpoint URL:

+-------------+---------------------------------------------------+
| Method Type | Url                                               |
+=============+===================================================+
| GET         | portal.brighte.com.au/rest/vendor/v1/applications |
+-------------+---------------------------------------------------+

Parameters:

+--------------+---------+---------------------------------------------+
| Name         | Value   | Description                                 |
+==============+=========+=============================================+
| accountId    | string  | Id of the account to list applications for. |
+--------------+---------+---------------------------------------------+
| *startDate*  | date    | *(optional, default beginning)* Start Date  |
+--------------+---------+---------------------------------------------+
| *endDate*    | date    | *(optional, default today)* End Date        |
+--------------+---------+---------------------------------------------+
| *pageNumber* | integer | *(optional, default 1)* Page Number         |
+--------------+---------+---------------------------------------------+

Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/applications HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "applications": [
            Application POD
        ],
        "hasMoreRecords": boolean
    }

Resources:

* `Application POD <dataTypes.html#application>`_

Error codes:

+------+--------+-------------------------+----------------------------------------------------------------------+
| Code | Status | Message                 | Reason                                                               |
+======+========+=========================+======================================================================+
| 401  | 4010   | Not authorized.         | You are not authorized to perform this action with given parameters. |
+------+--------+-------------------------+----------------------------------------------------------------------+
| 400  | 4402   | Invalid date input.     | Wrong `startDate` or `endDate` input.                                |
+------+--------+-------------------------+----------------------------------------------------------------------+
| 400  | 4402   | Invalid application ID. | Please specify an application id.                                    |
+------+--------+-------------------------+----------------------------------------------------------------------+
| 404  | 4041   | Invalid application ID. | Application not found.                                               |
+------+--------+-------------------------+----------------------------------------------------------------------+
| 500  | 5000   | Internal error.         | There was an error with this request, contact support.               |
+------+--------+-------------------------+----------------------------------------------------------------------+
| 401  | 4010   | Invalid application ID. | You are not authorized to access this resource.                      |
+------+--------+-------------------------+----------------------------------------------------------------------+


------------------------------------------------------------------------------------------------------------------------

Get Application
---------------

Return information about a specific application.

Endpoint URL:

+-------------+--------------------------------------------------------------------+
| Method Type | Url                                                                |
+=============+====================================================================+
| GET         | portal.brighte.com.au/rest/vendor/v1/applications/{application_id} |
+-------------+--------------------------------------------------------------------+

Parameters:

+----------------+--------+-----------------------+
| Name           | Value  | Description           |
+================+========+=======================+
| application_id | string | Id of the application |
+----------------+--------+-----------------------+

Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/applications/{application_id} HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "application": "Application POD"
    }

Resources:

* `Application POD <dataTypes.html#application>`_

Error codes:

+------+--------+-------------------------+--------------------------------------------------------+
| Code | Status | Message                 | Reason                                                 |
+======+========+=========================+========================================================+
| 400  | 4402   | Invalid application ID. | Please specify an application id.                      |
+------+--------+-------------------------+--------------------------------------------------------+
| 404  | 4041   | Invalid application ID. | Application not found.                                 |
+------+--------+-------------------------+--------------------------------------------------------+
| 500  | 5000   | Internal error.         | There was an error with this request, contact support. |
+------+--------+-------------------------+--------------------------------------------------------+
| 401  | 4010   | Invalid application ID. | You are not authorized to access this resource.        |
+------+--------+-------------------------+--------------------------------------------------------+



------------------------------------------------------------------------------------------------------------------------

Update Application
------------------

Edit an existing application. It is only possible to update the `reference_number`.

Endpoint URL:

+-------------+-------------------------------------------------------------------+
| Method Type | Url                                                               |
+=============+===================================================================+
| PUT         | portal.brighte.com.au/rest/vendor/v1/application/{application_id} |
+-------------+-------------------------------------------------------------------+

Parameters:

+------------------+--------+------------------------------------------+
| Name             | Value  | Description                              |
+==================+========+==========================================+
| reference_number | string | New reference number of the application. |
+------------------+--------+------------------------------------------+

Sample request:

HTTP

.. code-block:: http

    PUT /rest/vendor/v1/application/{application_id} HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

    {
        "reference_number": "NEW_REF_NUM"
    }


You should recieve the following response.

Successful Sample Response:

.. code-block:: json

    {
        "status": 2000,
        "application": "Application POD"
    }

Resources:

* `Application POD <dataTypes.html#application>`_

Error codes:

+------+--------+-------------------------+--------------------------------------------------------+
| Code | Status | Message                 | Reason                                                 |
+======+========+=========================+========================================================+
| 400  | 4402   | Invalid application ID. | Please specify an application id.                      |
+------+--------+-------------------------+--------------------------------------------------------+
| 404  | 4041   | Invalid application ID. | Application not found.                                 |
+------+--------+-------------------------+--------------------------------------------------------+
| 500  | 5000   | Internal error.         | There was an error with this request, contact support. |
+------+--------+-------------------------+--------------------------------------------------------+
| 401  | 4010   | Invalid application ID. | You are not authorized to access this resource.        |
+------+--------+-------------------------+--------------------------------------------------------+


------------------------------------------------------------------------------------------------------------------------

Get Application Document Types
------------------------------

Retrieve all the possible document types for current user.


Endpoint URL:

+-------------+-----------------------------------------------------------------+
| Method Type | Url                                                             |
+=============+=================================================================+
| GET         | portal.brighte.com.au/rest/vendor/v1/application/document-types |
+-------------+-----------------------------------------------------------------+


Parameters:

*No parameters*


Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/application/document-types HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "applicationDocumentTypes": [
            "invoice",
            "CCA"
        ]
    }

Resources:

* `applicationDocumentType ENUM <dataTypes.html#enums>`_

Error codes:

+------+--------+-----------------+----------------------------------+
| Code | Status | Message         | Reason                           |
+======+========+=================+==================================+
| 500  | 4000   | Internal Error. | Internal error, contact support. |
+------+--------+-----------------+----------------------------------+

------------------------------------------------------------------------------------------------------------------------

Get Application Documents
-------------------------

Retrieve all the documents (invoices, ccas, stcs...) associated with a certain application.


Endpoint URL:

+-------------+----------------------------------------------------------------------------+
| Method Type | Url                                                                        |
+=============+============================================================================+
| GET         | portal.brighte.com.au/rest/v1/vendor/application/{application_id}/document |
+-------------+----------------------------------------------------------------------------+


Parameters:

+----------------+--------+-----------------------+
| Name           | Value  | Description           |
+================+========+=======================+
| application_id | string | Id of the application |
+----------------+--------+-----------------------+


Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/application/{application_id}/document HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache



Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "applicationDocuments": [
            "ApplicationDocument POD"
        ]
    }

Resources:

* `Application Document POD <dataTypes.html#applicationdocument>`_

Error codes:

+------+--------+-------------------------+---------------------------------------------------------------+
| Code | Status | Message                 | Reason                                                        |
+======+========+=========================+===============================================================+
| 400  | 4402   | Invalid application ID. | Please specify an application id.                             |
+------+--------+-------------------------+---------------------------------------------------------------+
| 404  | 4041   | Invalid application ID. | Application not found.                                        |
+------+--------+-------------------------+---------------------------------------------------------------+
| 500  | 5000   | Internal error.         | There was an error with this request, contact support.        |
+------+--------+-------------------------+---------------------------------------------------------------+
| 401  | 4010   | Invalid application ID. | You are not authorized to access this resource.               |
+------+--------+-------------------------+---------------------------------------------------------------+



------------------------------------------------------------------------------------------------------------------------

Create Application Document
---------------------------

Upload documents (invoices, STCs, CCAs ... ) to the given application.


Endpoint URL:

+-------------+----------------------------------------------------------------------------+
| Method Type | Url                                                                        |
+=============+============================================================================+
| POST        | portal.brighte.com.au/rest/v1/vendor/application/{application_id}/document |
+-------------+----------------------------------------------------------------------------+


Parameters:

+----------------+--------+-------------------------------------------------------------------------------------+
| Name           | Value  | Description                                                                         |
+================+========+=====================================================================================+
| application_id | string | Id of the application                                                               |
+----------------+--------+-------------------------------------------------------------------------------------+
| invoice_type   | string | Type of the document, one of `applicationDocumentType ENUM <dataTypes.html#enums>`_ |
+----------------+--------+-------------------------------------------------------------------------------------+
| filename       | string | Name of the file, allowed types are **'jpg', 'jpeg', 'png', 'pdf'**                 |
+----------------+--------+-------------------------------------------------------------------------------------+
| filecontent    | string | BASE64 encoded content of the docuemnt                                              |
+----------------+--------+-------------------------------------------------------------------------------------+


Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/application/{application_id}/document HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

    {
        "invoice_type": "invoice",
        "filename": "invoice.jpg",
        "filecontent" : "xxx"
    }


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "applicationDocument": ApplicationDocument POD,
    }

Resources:

* `applicationDocumentType ENUM <dataTypes.html#enums>`_
* `Application Document POD <dataTypes.html#applicationdocument>`_

Error codes:

+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| Code | Status | Message                                | Reason                                                                                     |
+======+========+========================================+============================================================================================+
| 400  | 4402   | Invalid application ID.                | Please specify an application id.                                                          |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 404  | 4041   | Invalid application ID.                | Application not found.                                                                     |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 500  | 5000   | Internal error.                        | There was an error with this request, contact support.                                     |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 401  | 4010   | Invalid application ID.                | You are not authorized to access this resource.                                            |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 400  | 4402   | Invalid application document type.     | The document type has to be one of `applicationDocumentType ENUM <dataTypes.html#enums>`_. |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 400  | 4010   | No document type specified.            | You have to specify the application document type via invoice_type parameter.              |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 400  | 4010   | Invalid filename.                      | You have to specify the document filename.                                                 |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 400  | 4010   | Invalid filetype.                      | Allowed types are 'jpg', 'jpeg', 'png', 'pdf'.                                             |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 400  | 4402   | File content has to be base64 encoded. | Encode the content with BASE64.                                                            |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 500  | 4000   | Internal Error.                        | Failure, contact support.                                                                  |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+


------------------------------------------------------------------------------------------------------------------------

Request Application Payment
---------------------------

After the succesfull delivery and installment of the product at the physical location of the buyer by the installer, this method should be invoked to indicate that the buyer is ready to be started on the repayments.

Before he is actually started on the repayments however, the buyer is prompted to confirm the succesfull delivery and installment as well.


Endpoint URL:

+-------------+-----------------------------------------------------------------------------------+
| Method Type | Url                                                                               |
+=============+===================================================================================+
| POST        | portal.brighte.com.au/rest/vendor/v1/application/{application_id}/request-payment |
+-------------+-----------------------------------------------------------------------------------+


Parameters:

+----------------+--------+-----------------------+
| Name           | Value  | Description           |
+================+========+=======================+
| application_id | string | Id of the application |
+----------------+--------+-----------------------+


Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/application/{application_id}/request-payment HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache


Succesfull response:

Successful Sample Response:

.. code-block:: json

    {
        "status": 2000,
        "application": "Application POD"
    }

Resources:

* `Application POD <dataTypes.html#application>`_

Error codes:

+------+--------+-------------------------+--------------------------------------------------------+
| Code | Status | Message                 | Reason                                                 |
+======+========+=========================+========================================================+
| 401  | 4403   | Unauthorized.           | You are not authorized to access this resource.        |
+------+--------+-------------------------+--------------------------------------------------------+
| 404  | 4041   | Not found.              | Invalid application id.                                |
+------+--------+-------------------------+--------------------------------------------------------+
| 500  | 5000   | Error occured.          | There was an error with this request, contact support. |
+------+--------+-------------------------+--------------------------------------------------------+
| 400  | 4402   | Invalid application ID. | Please specify an application id.                      |
+------+--------+-------------------------+--------------------------------------------------------+
| 404  | 4041   | Invalid application ID. | Application not found.                                 |
+------+--------+-------------------------+--------------------------------------------------------+
| 500  | 5000   | Internal error.         | There was an error with this request, contact support. |
+------+--------+-------------------------+--------------------------------------------------------+
| 401  | 4010   | Invalid application ID. | You are not authorized to access this resource.        |
+------+--------+-------------------------+--------------------------------------------------------+



------------------------------------------------------------------------------------------------------------------------

Send Application Secure Code
----------------------------

Send application secure code to the applicant’s mobile phone. This API call will return a random token that will need to be submitted along with the secure code.

Endpoint URL:

+-------------+--------------------------------------------------------------+
| Method Type | Url                                                          |
+=============+==============================================================+
| POST        | portal.brighte.com.au/rest/vendor/v1/application/secure-code |
+-------------+--------------------------------------------------------------+

Parameters:

+--------+--------+-----------------------------------------------------------------+
| Name   | Value  | Description                                                     |
+========+========+=================================================================+
| mobile | string | The mobile phone number as a plain string without country code. |
+--------+--------+-----------------------------------------------------------------+

Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/application/secure-code HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

    {
      "mobile" : "0416123456"
    }


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "token": "072b17a51edf4ca38f836293f3018f7117fa2c88"
    }

Error codes:

+------+--------+-------------------------------+--------------------------------------------------------+
| Code | Status | Message                       | Reason                                                 |
+======+========+===============================+========================================================+
| 400  | 4403   | Invalid Input.                | You are not authorized to access this resource.        |
+------+--------+-------------------------------+--------------------------------------------------------+
| 404  | 4040   | Failed to generate auth code. | Failed to generate code, contact support.              |
+------+--------+-------------------------------+--------------------------------------------------------+
| 500  | 4000   | Failed to process auth code.  | Failed to process code, contact support.               |
+------+--------+-------------------------------+--------------------------------------------------------+
| 500  | 5000   | Error occured.                | There was an error with this request, contact support. |
+------+--------+-------------------------------+--------------------------------------------------------+


------------------------------------------------------------------------------------------------------------------------

Quotes
~~~~~~

Get Quotes
----------

Return all the quotes for a specific account.

The results are ordered by creation date. There is a maximum limit of 500 records per call and the next result set can be retrieved by utilizing the `pageNumber` parameter and the `hasMoreRecords` return field.

Endpoint URL:

+-------------+------------------------------------------+
| Method Type | Url                                      |
+=============+==========================================+
| GET         | portal.brighte.com.au/rest/vendor/quotes |
+-------------+------------------------------------------+

Parameters:

+--------------+---------+--------------------------------------------+
| Name         | Value   | Description                                |
+==============+=========+============================================+
| accountId    | string  | Id of the account to get the quotes for.   |
+--------------+---------+--------------------------------------------+
| *startDate*  | date    | *(optional, default beginning)* Start Date |
+--------------+---------+--------------------------------------------+
| *endDate*    | date    | *(optional, default today)* End Date       |
+--------------+---------+--------------------------------------------+
| *pageNumber* | integer | *(optional, default 1)* Page Number        |
+--------------+---------+--------------------------------------------+


Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/quotes?startDate=2018-04-01&endDate=2018-05-04&accountId=E897.675 HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "quotes": [
            Quote POD
        ],
        "hasMoreRecords": boolean
    }

Resources:

* `Quote POD <dataTypes.html#quote>`_

Error codes:

+------+--------+-------------------+----------------------------------------------------------------------------+
| Code | Status | Message           | Reason                                                                     |
+======+========+===================+============================================================================+
| 400  | 4402   | Invalid date.     | Wrong `startDate` or `endDate` input.                                      |
+------+--------+-------------------+----------------------------------------------------------------------------+
| 401  | 4010   | Not authorized.   | You are not authorized to perform this action with given parameters.       |
+------+--------+-------------------+----------------------------------------------------------------------------+
| 400  | 4402   | Invalid account.  | Wrong account id provided. See `Get Accounts <restful.html#get-accounts>`_ |
+------+--------+-------------------+----------------------------------------------------------------------------+
| 500  | 5000   | Internal error.   | Internal error occured, contact support.                                   |
+------+--------+-------------------+----------------------------------------------------------------------------+


------------------------------------------------------------------------------------------------------------------------

Get Quote
---------

Return information about a specific quote.

Endpoint URL:

+-------------+-------------------------------------+
| Method Type | Url                                 |
+=============+=====================================+
| GET         | api.brighte/rest/vendor/quotes/{id} |
+-------------+-------------------------------------+

Parameters:

+------+--------+------------------+
| Name | Value  | Description      |
+======+========+==================+
| id   | string | Id of the quote. |
+------+--------+------------------+


Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/quotes/1 HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "quote": Quote POD
    }

Resources:

* `Quote POD <dataTypes.html#quote>`_

Error codes:

+------+--------+-------------------+--------------------------------------------------------+
| Code | Status | Message           | Reason                                                 |
+======+========+===================+========================================================+
| 400  | 4402   | Invalid quote ID. | Please specify a quote id.                             |
+------+--------+-------------------+--------------------------------------------------------+
| 404  | 4041   | Invalid quote ID. | Quote not found.                                       |
+------+--------+-------------------+--------------------------------------------------------+
| 500  | 5000   | Internal error.   | There was an error with this request, contact support. |
+------+--------+-------------------+--------------------------------------------------------+
| 401  | 4010   | Invalid quote ID. | You are not authorized to access this resource.        |
+------+--------+-------------------+--------------------------------------------------------+

------------------------------------------------------------------------------------------------------------------------

Create Quote
------------

Create a quote.

*You need to specify an account id.*

Endpoint URL:

+-------------+--------------------------------------------+
| Method Type | Url                                        |
+=============+============================================+
| POST        | portal.brighte.com.au/rest/vendor/v1/quote |
+-------------+--------------------------------------------+

Parameters:

+-----------------------+----------+--------------------------------------------------+
| Name                  | Value    | Description                                      |
+=======================+==========+==================================================+
| account_id            | string   | Account id of the branch that this quote is for. |
+-----------------------+----------+--------------------------------------------------+
| first_name            | string   | First name of the quote receiver.                |
+-----------------------+----------+--------------------------------------------------+
| last_name             | string   | Last name of the quote receiver.                 |
+-----------------------+----------+--------------------------------------------------+
| phone                 | string   | Phone of the quote receiver.                     |
+-----------------------+----------+--------------------------------------------------+
| email                 | string   | Email of the quote receiver.                     |
+-----------------------+----------+--------------------------------------------------+
| total_purchase_amount | currency | The total amount of the quote.                   |
+-----------------------+----------+--------------------------------------------------+
| deposit_amount        | currency | The deposit of the quote.                        |
+-----------------------+----------+--------------------------------------------------+
| repayment_term        | integer  | Repayment term in months.                        |
+-----------------------+----------+--------------------------------------------------+
| product_category      | string   | Category  of the product in this application.    |
+-----------------------+----------+--------------------------------------------------+
| product_description   | string   | Description of the financed items.               |
+-----------------------+----------+--------------------------------------------------+


Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/quote HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

    {
        "first_name" : "Test First",
        "last_name" : "Lastnametest",
        "phone" : "0434632777",
        "email" : "generic.test.case420@gmail.com",
        "total_purchase_amount" : 15000,
        "deposit_amount" : 5000,
        "repayment_term" : 48,
        "product_category" : "Storage Battery(s)",
        "product_description" : "This product stores solar energy",
        "account_id" : "E435"
    }


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "quote": Quote POD
    }

Resources:

* `Quote POD <dataTypes.html#quote>`_

Error codes:

+------+--------+------------------------+--------------------------------------------------------------------------+
| Code | Status | Message                | Reason                                                                   |
+======+========+========================+==========================================================================+
| 400  | 4402   | Invalid account id.    | Wrong account id specified.                                              |
+------+--------+------------------------+--------------------------------------------------------------------------+
| 401  | 4010   | Not authorized.        | You are not authorized, contact support.                                 |
+------+--------+------------------------+--------------------------------------------------------------------------+
| 400  | 4402   | Invalid data provided. | Invalid quote data provided in the request, see details in the response. |
+------+--------+------------------------+--------------------------------------------------------------------------+
| 500  | 5000   | Internal error.        | Internal error occured, contact support.                                 |
+------+--------+------------------------+--------------------------------------------------------------------------+


------------------------------------------------------------------------------------------------------------------------

Resend Quote
------------

Resend a specific quote.

Endpoint URL:

+-------------+-------------------------------------------+
| Method Type | Url                                       |
+=============+===========================================+
| GET         | api.brighte/rest/vendor/quote/{id}/resend |
+-------------+-------------------------------------------+

Parameters:

+------+--------+------------------+
| Name | Value  | Description      |
+======+========+==================+
| id   | string | Id of the quote. |
+------+--------+------------------+


Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/quote/{id}/resend HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "quote": Quote POD
    }

Resources:

* `Quote POD <dataTypes.html#quote>`_

Error codes:

+------+--------+------------------------+-----------------------------------------------------------------------------+
| Code | Status | Message                | Reason                                                                      |
+======+========+========================+=============================================================================+
| 400  | 4402   | Quote already applied. | Cant resend this quote, an application has already been filed with Brighte. |
+------+--------+------------------------+-----------------------------------------------------------------------------+
| 400  | 4402   | The quote is invalid.  | There is a problem with the quote, please create a new one.                 |
+------+--------+------------------------+-----------------------------------------------------------------------------+
| 400  | 4402   | The quote is expired.  | The quote is expired already, please create a new one.                      |
+------+--------+------------------------+-----------------------------------------------------------------------------+
| 400  | 4402   | Invalid quote ID.      | Please specify a quote id.                                                  |
+------+--------+------------------------+-----------------------------------------------------------------------------+
| 404  | 4041   | Invalid quote ID.      | Quote not found.                                                            |
+------+--------+------------------------+-----------------------------------------------------------------------------+
| 500  | 5000   | Internal error.        | There was an error with this request, contact support.                      |
+------+--------+------------------------+-----------------------------------------------------------------------------+
| 401  | 4010   | Invalid quote ID.      | You are not authorized to access this resource.                             |
+------+--------+------------------------+-----------------------------------------------------------------------------+


------------------------------------------------------------------------------------------------------------------------

Delete Quote
------------

Cancel/delete a specific quote.

Endpoint URL:

+-------------+------------------------------------+
| Method Type | Url                                |
+=============+====================================+
| DELETE      | api.brighte/rest/vendor/quote/{id} |
+-------------+------------------------------------+

Parameters:

+------+--------+------------------+
| Name | Value  | Description      |
+======+========+==================+
| id   | string | Id of the quote. |
+------+--------+------------------+


Sample request:

HTTP

.. code-block:: http

    DELETE /rest/vendor/v1/quote/{id} HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "quote": Quote POD
    }

Resources:

* `Quote POD <dataTypes.html#quote>`_

Error codes:

+------+--------+-------------------+--------------------------------------------------------+
| Code | Status | Message           | Reason                                                 |
+======+========+===================+========================================================+
| 400  | 4402   | Quote not active. | Can't cancel a quote that is expired or inactive.      |
+------+--------+-------------------+--------------------------------------------------------+
| 400  | 4402   | Invalid quote ID. | Please specify a quote id.                             |
+------+--------+-------------------+--------------------------------------------------------+
| 404  | 4041   | Invalid quote ID. | Quote not found.                                       |
+------+--------+-------------------+--------------------------------------------------------+
| 500  | 5000   | Internal error.   | There was an error with this request, contact support. |
+------+--------+-------------------+--------------------------------------------------------+
| 401  | 4010   | Invalid quote ID. | You are not authorized to access this resource.        |
+------+--------+-------------------+--------------------------------------------------------+


------------------------------------------------------------------------------------------------------------------------

Users
~~~~~

Get Users
---------

The results are ordered by creation date. There is a maximum limit of 500 records per call and the next result set can be retrieved by utilizing the `pageNumber` parameter and the `hasMoreRecords` return field.

Endpoint URL:

+-------------+--------------------------------------------+
| Method Type | Url                                        |
+=============+============================================+
| GET         | portal.brighte.com.au/rest/vendor/v1/users |
+-------------+--------------------------------------------+

Parameters:

+--------------+---------+--------------------------------------------+
| Name         | Value   | Description                                |
+==============+=========+============================================+
| accountId    | string  | Account to retrieve the users for.         |
+--------------+---------+--------------------------------------------+
| *startDate*  | date    | *(optional, default beginning)* Start Date |
+--------------+---------+--------------------------------------------+
| *endDate*    | date    | *(optional, default today)* End Date       |
+--------------+---------+--------------------------------------------+
| *pageNumber* | integer | *(optional, default 1)* Page Number        |
+--------------+---------+--------------------------------------------+

Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/users?accountId=xxx&startDate=2018-01-01&endDate=2018-01-31&pageNumber=1 HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache


Successful Sample Response:

.. code-block:: json

    {
        "status": 2000,
        "users": [
            User POD
        ],
        "hasMoreRecords": boolean
    }

Resources:

* `User POD <dataTypes.html#user>`_

Error codes:

+------+--------+---------------------+--------------------------------------------------------+
| Code | Status | Message             | Reason                                                 |
+======+========+=====================+========================================================+
| 401  | 4010   | Invalid account id. | Please specify a valid account id.                     |
+------+--------+---------------------+--------------------------------------------------------+

------------------------------------------------------------------------------------------------------------------------

Get User
--------

Return information about a specific user.

Endpoint URL:

+-------------+-----------------------------------------------------+
| Method Type | Url                                                 |
+=============+=====================================================+
| GET         | portal.brighte.com.au/rest/vendor/v1/user/{user_id} |
+-------------+-----------------------------------------------------+

Parameters:

+--------+--------+-------------------+
| Name   | Value  | Description       |
+========+========+===================+
| userId | string | User to retrieve. |
+--------+--------+-------------------+

Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/user/{user_id} HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

Successful Sample Response:

.. code-block:: json

    {
        "status": 2000,
        "user": "User POD"
    }

Resources:

* `User POD <dataTypes.html#user>`_

Error codes:

+------+--------+------------------+--------------------------------------------------------+
| Code | Status | Message          | Reason                                                 |
+======+========+==================+========================================================+
| 400  | 4402   | Invalid user ID. | Please specify a user id.                              |
+------+--------+------------------+--------------------------------------------------------+
| 404  | 4041   | Invalid user ID. | User not found.                                        |
+------+--------+------------------+--------------------------------------------------------+
| 500  | 5000   | Internal error.  | There was an error with this request, contact support. |
+------+--------+------------------+--------------------------------------------------------+
| 401  | 4010   | Invalid user ID. | You are not authorized to access this resource.        |
+------+--------+------------------+--------------------------------------------------------+


------------------------------------------------------------------------------------------------------------------------

Create User
-----------

Create a user with a specific account and user role.

Endpoint URL:

+-------------+-----------------------------------------------+
| Method Type | Url                                           |
+=============+===============================================+
| POST        | portal.brighte.com.au/rest/vendor/v1/user/add |
+-------------+-----------------------------------------------+

 Parameters:

+------------+--------+-------------------------------------------+
| Name       | Value  | Description                               |
+============+========+===========================================+
| account_id | string | Account ID.                               |
+------------+--------+-------------------------------------------+
| role       | enum   | See userRole.                             |
+------------+--------+-------------------------------------------+
| first_name | string | Users first name.                         |
+------------+--------+-------------------------------------------+
| last_name  | string | Users last name.                          |
+------------+--------+-------------------------------------------+
| email      | string | Unique email of the user.                 |
+------------+--------+-------------------------------------------+
| mobile     | string | Mobile phone number without country code. |
+------------+--------+-------------------------------------------+

Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/user/add HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

    {
        "role": "team+member",
        "first_name": "Testfn",
        "last_name": "TestLn",
        "email": "first.last1536623737@brighte.com.au",
        "mobile": "0411085823",
        "account_id": "E81"
    }

Successful Sample Response:

.. code-block:: json

    {
        "status": 2000,
        "user": "User POD"
    }

Resources:

* `User POD <dataTypes.html#user>`_

Error codes:

+------+--------+------------------------+-------------------------------------------------+
| Code | Status | Message                | Reason                                          |
+======+========+========================+=================================================+
| 401  | 4010   | Invalid account id.    | Please specify a valid account id.              |
+------+--------+------------------------+-------------------------------------------------+
| 401  | 4010   | Not authorized.        | You are not authorized to access this resource. |
+------+--------+------------------------+-------------------------------------------------+
| 400  | 4402   | Invalid role.          | Please specify a valid role.                    |
+------+--------+------------------------+-------------------------------------------------+
| 400  | 4402   | Invalid data provided. | See `details` in the response.                  |
+------+--------+------------------------+-------------------------------------------------+

------------------------------------------------------------------------------------------------------------------------

Update User
-----------

Update a user.

Endpoint URL:

+-------------+----------------------------------------------------+
| Method Type | Url                                                |
+=============+====================================================+
| PUT         | portal.brighte.com.au/rest/vendor/v1/user/{userId} |
+-------------+----------------------------------------------------+

Parameters:

+------------+--------+-------------------------------------------+
| Name       | Value  | Description                               |
+============+========+===========================================+
| userId     | string | User to update.                           |
+------------+--------+-------------------------------------------+
| role       | enum   | See userRole.                             |
+------------+--------+-------------------------------------------+
| first_name | string | Users first name.                         |
+------------+--------+-------------------------------------------+
| last_name  | string | Users last name.                          |
+------------+--------+-------------------------------------------+
| email      | string | Unique email of the user.                 |
+------------+--------+-------------------------------------------+
| mobile     | string | Mobile phone number without country code. |
+------------+--------+-------------------------------------------+

Sample request:

HTTP

.. code-block:: http

    PUT /rest/vendor/v1/user/{userId} HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

    {
        "first_name": "johnny",
        "last_name": "bravo"
    }

Successful Sample Response:

.. code-block:: json

    {
        "status": 2000,
        "user": "User POD"
    }

Resources:

* `User POD <dataTypes.html#user>`_

Error codes:

+------+--------+------------------------+-------------------------------------------------+
| Code | Status | Message                | Reason                                          |
+======+========+========================+=================================================+
| 401  | 4010   | Not authorized.        | You are not authorized to access this resource. |
+------+--------+------------------------+-------------------------------------------------+
| 400  | 4402   | Invalid role.          | Please specify a valid role.                    |
+------+--------+------------------------+-------------------------------------------------+
| 400  | 4402   | Invalid data provided. | See `details` in the response.                  |
+------+--------+------------------------+-------------------------------------------------+
| 403  | 4403   | Action not allowed.    | Contact support for more explanation.           |
+------+--------+------------------------+-------------------------------------------------+

------------------------------------------------------------------------------------------------------------------------

Delete User
-----------

Delete a specific user.

Endpoint URL:

+-------------+----------------------------------------------------+
| Method Type | Url                                                |
+=============+====================================================+
| DELETE      | portal.brighte.com.au/rest/vendor/v1/user/{userId} |
+-------------+----------------------------------------------------+

Parameters:

+--------+--------+-----------------+
| Name   | Value  | Description     |
+========+========+=================+
| userId | string | User to delete. |
+--------+--------+-----------------+

Sample request:

HTTP

.. code-block:: http

    DELETE /rest/vendor/v1/user/{userId} HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

Successful Sample Response:

.. code-block:: json

    {
        "status": "2000"
    }

Error codes:

+------+--------+------------------+--------------------------------------------------------+
| Code | Status | Message          | Reason                                                 |
+======+========+==================+========================================================+
| 400  | 4402   | Invalid user ID. | Please specify a user id.                              |
+------+--------+------------------+--------------------------------------------------------+
| 404  | 4041   | Invalid user ID. | User not found.                                        |
+------+--------+------------------+--------------------------------------------------------+
| 500  | 5000   | Internal error.  | There was an error with this request, contact support. |
+------+--------+------------------+--------------------------------------------------------+
| 401  | 4010   | Invalid user ID. | You are not authorized to access this resource.        |
+------+--------+------------------+--------------------------------------------------------+

------------------------------------------------------------------------------------------------------------------------

Generate User Api Credentials
-----------------------------

Generate API-Id and API-Secret for a specific user.

Endpoint URL:

+-------------+-----------------------------------------------------------------------------+
| Method Type | Url                                                                         |
+=============+=============================================================================+
| POST        | portal.brighte.com.au/rest/vendor/v1/user/{userId}/generate-api-credentials |
+-------------+-----------------------------------------------------------------------------+

Parameters:

+------------+--------+-------------------------------------------+
| Name       | Value  | Description                               |
+============+========+===========================================+
| userId     | string | User to generate the api credentials for. |
+------------+--------+-------------------------------------------+
| account_id | string | Credential to access this account.        |
+------------+--------+-------------------------------------------+

Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/user/{userId}/generate-api-credentials HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

    {
        "accountId":"E81"
    }

Successful Sample Response:

.. code-block:: json

    {
       "status": 2000,
       "userKey": "UserKey POD"
    }


Resources:

* `UserKey POD <dataTypes.html#userkey>`_

Error codes:

+------+--------+------------------------+--------------------------------------------------------+
| Code | Status | Message                | Reason                                                 |
+======+========+========================+========================================================+
| 401  | 4010   | Not authorized.        | You are not authorized to access this resource.        |
+------+--------+------------------------+--------------------------------------------------------+
| 400  | 4402   | Invalid user ID.       | Please specify a user id.                              |
+------+--------+------------------------+--------------------------------------------------------+
| 404  | 4041   | Invalid user ID.       | User not found.                                        |
+------+--------+------------------------+--------------------------------------------------------+
| 500  | 5000   | Internal error.        | There was an error with this request, contact support. |
+------+--------+------------------------+--------------------------------------------------------+
| 401  | 4010   | Invalid user ID.       | You are not authorized to access this resource.        |
+------+--------+------------------------+--------------------------------------------------------+
| 400  | 4402   | Invalid data provided. | See `details` in the response.                         |
+------+--------+------------------------+--------------------------------------------------------+

------------------------------------------------------------------------------------------------------------------------

Content
~~~~~~~

Get Content
-----------

Retrieve documents such as privacy policy, terms and condition or direct debit agreement.

Endpoint URL:

+-------------+------------------------------------------------------------+
| Method Type | Url                                                        |
+=============+============================================================+
| GET         | portal.brighte.com.au/rest/vendor/v1/content/{contentType} |
+-------------+------------------------------------------------------------+

Parameters:

+-------------+-------+-------------------+
| Name        | Value | Description       |
+=============+=======+===================+
| contentType | enum  | Type of document. |
+-------------+-------+-------------------+

Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/content/{contentType} HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

Successful Sample Response:

.. code-block:: json

    {
        "status": 2000,
        "content": Content POD
    }

Resources:

* `Content POD <dataTypes.html#content>`_

Error codes:

+------+--------+----------------+---------------------------+
| Code | Status | Message        | Reason                    |
+======+========+================+===========================+
| 400  | 4402   | Invalid input. | Invalid input parameters. |
+------+--------+----------------+---------------------------+


------------------------------------------------------------------------------------------------------------------------

Product Categories
~~~~~~~~~~~~~~~~~~

Get Product Categories
----------------------

Return all possible values for product category.

Endpoint URL:

+-------------+---------------------------------------------------------+
| Method Type | Url                                                     |
+=============+=========================================================+
| GET         | portal.brighte.com.au/rest/vendor/v1/product-categories |
+-------------+---------------------------------------------------------+

Parameters:

*No parameters*

Sample request:

HTTP

.. code-block:: http

    GET /rest/vendor/v1/product-categories HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

Successful Sample Response:

.. code-block:: json

    {
        "status": 2000,
        "productCategories": [
            ProductCategory POD
        ]
    }

Resources:

* `ProductCategory POD <dataTypes.html#productcategory>`_

Error codes:

*No error codes*

------------------------------------------------------------------------------------------------------------------------

Calculate Repayment Amount
~~~~~~~~~~~~~~~~~~~~~~~~~~

Calculate Repayment Amount
--------------------------

Calculate the repayment amount of the loan.

Endpoint URL:

+-------------+-----------------------------------------------------------------+
| Method Type | Url                                                             |
+=============+=================================================================+
| POST        | portal.brighte.com.au/rest/v1/vendor/calculate-repayment-amount |
+-------------+-----------------------------------------------------------------+


Parameters:

+---------------------+----------+----------------------------------------------------------------------------+
| Name                | Value    | Description                                                                |
+=====================+==========+============================================================================+
| totalPurchaseAmount | currency | Total amount of the loan.                                                  |
+---------------------+----------+----------------------------------------------------------------------------+
| depositAmount       | currency | The deposit of the loan, will be substracted from the total amount.        |
+---------------------+----------+----------------------------------------------------------------------------+
| repaymentTerm       | integer  | The repayment term in months.                                              |
+---------------------+----------+----------------------------------------------------------------------------+
| includeFees         | boolean  | Whether to include the account establishment fee for first time customers. |
+---------------------+----------+----------------------------------------------------------------------------+

Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/calculate-repayment-amount HTTP/1.1
    Host: brighte.localhost
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Accept: application/json
    Cache-Control: no-cache

    {
        "totalPurchaseAmount" : 15000,
        "depositAmount": 1000,
        "repaymentTerm" : 48,
        "includeFees" : 1
    }


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "repaymentAmount": RepaymentAmount POD
    }

Resources:

* `RepaymentAmount POD <dataTypes.html#repaymentamount>`_

Error codes:

+------+--------+----------------+---------------------------+
| Code | Status | Message        | Reason                    |
+======+========+================+===========================+
| 400  | 4402   | Invalid input. | Invalid input parameters. |
+------+--------+----------------+---------------------------+


------------------------------------------------------------------------------------------------------------------------

Callout Requests
~~~~~~~~~~~~~~~~

Create Callout Request
----------------------

Initiate a callout request. If successful, the call will be made at the earliest available date and time, described in the returned `call_datetime` field.

Endpoint URL:

+-------------+-------------------------------------------------------+
| Method Type | Url                                                   |
+=============+=======================================================+
| POST        | portal.brighte.com.au/rest/vendor/v1/callout-requests |
+-------------+-------------------------------------------------------+

Parameters:

+-----------------------+----------+-----------------------------------------------+
| Name                  | Value    | Description                                   |
+=======================+==========+===============================================+
| first_name            | string   | First name of the client.                     |
+-----------------------+----------+-----------------------------------------------+
| last_name             | string   | Last name of the client.                      |
+-----------------------+----------+-----------------------------------------------+
| phone                 | string   | Phone number of the client.                   |
+-----------------------+----------+-----------------------------------------------+
| email                 | string   | Email of the client.                          |
+-----------------------+----------+-----------------------------------------------+
| total_purchase_amount | currency | Proposed financed amount.                     |
+-----------------------+----------+-----------------------------------------------+
| deposit_amount        | currency | Proposed deposit amount.                      |
+-----------------------+----------+-----------------------------------------------+
| repayment_term        | integer  | Repayment term in months.                     |
+-----------------------+----------+-----------------------------------------------+
| product_category      | string   | Category  of the product in this application. |
+-----------------------+----------+-----------------------------------------------+
| product_description   | string   | Description of the financed items.            |
+-----------------------+----------+-----------------------------------------------+

Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/callout-requests HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/json
    X-Session-Key: xxx
    X-Session-Token: xxx
    Cache-Control: no-cache

    {
        "account_id" : 3,
        "first_name" : "John",
        "last_name" : "Smith",
        "phone" : "0434090090",
        "email" : "email@gmail.com",
        "total_purchase_amount" : 15000,
        "deposit_amount" : 5000,
        "repayment_term" : 48,
        "product_category" : "Storage Battery(s)",
        "product_description" : "Excellent Battery"
    }

Successful Sample Response:

.. code-block:: json

    {
        "status": 2000,
        "callout": Callout POD
    }

Resources:

* `Callout POD <dataTypes.html#callout>`_

Error codes:

+------+--------+-----------------+------------------------------------------+
| Code | Status | Message         | Reason                                   |
+======+========+=================+==========================================+
| 400  | 4402   | Invalid input.  | Invalid input parameters.                |
+------+--------+-----------------+------------------------------------------+
| 401  | 4010   | Not authorized. | You are not authorized.                  |
+------+--------+-----------------+------------------------------------------+
| 500  | 5000   | Internal error. | Internal error occured, contact support. |
+------+--------+-----------------+------------------------------------------+

