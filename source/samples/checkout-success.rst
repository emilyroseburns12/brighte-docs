
.. code-block:: html

 <html>
 <head>
 <title>Some Solar Purchase</title>
 </head>
 <body>
     <h1>Some Solar</h1>
     <?php switch ($_GET['error_code']) : ?>
     <?php case 10000 : ?>
     <h2>Payment application submitted successfully.</h2>
     <p>You should receive an email from Brighte to complete your application.</p>
     <p>Your transaction ID is: <?php echo htmlspecialchars($_GET['transaction_id']); ?></p>
     <?php break; ?>
     <?php default: ?>
     <h2>Payment application failed.</h2>
     <p>Error: <?php echo htmlspecialchars($_GET['error_code']); ?> - <?php echo htmlspecialchars($_GET['error_msg']); ?></p>
 </body>
 </html>
