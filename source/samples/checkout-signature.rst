
.. code-block:: php

 if ($_GET['signature'] !== hash_hmac('sha256', $_GET['timestamp'] . $_GET['token'], utf8_encode(BRIGHTE_PRIVATE_KEY))) {
     exit;
 }

