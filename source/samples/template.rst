**template**


------------------------------------------------------------------------------------------------------------------------

Block Name
~~~~~~~~~~

MethodName
----------

Descriptive **Description** of the method, ideally with **use case**.


Endpoint URL:

+-------------+-------------------------------------------------------------------+
| Method Type | Url                                                               |
+=============+===================================================================+
| GET         | portal.brighte.com.au/rest/v1/vendor/application/{application_id} |
+-------------+-------------------------------------------------------------------+


Parameters:

+----------------+---------+--------------------------------------------+
| Name           | Value   | Description                                |
+================+=========+============================================+
| application_id | integer | Id of the application to retrieve          |
+----------------+---------+--------------------------------------------+
| *startDate*    | date    | *(optional, default beginning)* Start Date |
+----------------+---------+--------------------------------------------+
| *endDate*      | date    | *(optional, default today)* End Date       |
+----------------+---------+--------------------------------------------+
| *pageNumber*   | integer | *(optional, default 1)* Page Number        |
+----------------+---------+--------------------------------------------+


Sample request:

HTTP

.. code-block:: http

    POST /rest/vendor/v1/session HTTP/1.1
    Host: portal.brighte.com.au
    Accept: application/json
    Content-Type: application/x-www-form-urlencoded
    Cache-Control: no-cache
    Postman-Token: a48bf5b0-19b7-f58f-14a3-87ec6b825ae5

    apiId=xxx&apiSecret=xxx


Succesfull response:

.. code-block:: json

    {
        "status": 2000,
        "applications": [
            Application POD
        ],
        "hasMoreRecords": 0
    }

Resources:

* `Application POD <dataTypes.html#application>`_

Error codes:

+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| Code | Status | Message                                | Reason                                                                                     |
+======+========+========================================+============================================================================================+
| 404  | 4041   | Application not found.                 | Application not found in the system. Fix the application_id.                               |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 401  | 4010   | Unauthorized.                          | You are not authorized to perform this action.                                             |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 400  | 4402   | Invalid application document type.     | The document type has to be one of `applicationDocumentType ENUM <dataTypes.html#enums>`_. |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 400  | 4010   | No document type specified.            | You have to specify the application document type via invoice_type parameter.              |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 400  | 4010   | Invalid filename.                      | You have to specify the document filename.                                                 |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 400  | 4010   | Invalid filetype.                      | Allowed types are 'jpg', 'jpeg', 'png', 'pdf'.                                             |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 400  | 4402   | File content has to be base64 encoded. | Encode the content with BASE64.                                                            |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+
| 500  | 4000   | Failed to upload document.             | Failure, contact support.                                                                  |
+------+--------+----------------------------------------+--------------------------------------------------------------------------------------------+





PHP

.. code-block:: php

    <?php

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://brighte.localhost/rest/vendor/v1/session",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "POST",
      CURLOPT_POSTFIELDS => "",
      CURLOPT_HTTPHEADER => array(
        "accept: application/json",
        "cache-control: no-cache",
        "content-type: application/x-www-form-urlencoded",
        "postman-token: 8dea741d-979e-78ca-8ab3-63aca8f4fa1f"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      echo "cURL Error #:" . $err;
    } else {
      echo $response;
    }

PYTHON

.. code-block:: python

    import http.client

    conn = http.client.HTTPConnection("brighte.localhost")

    payload = ""

    headers = {
        'accept': "application/json",
        'content-type': "application/x-www-form-urlencoded",
        'cache-control': "no-cache",
        'postman-token': "3a4fb065-a266-d9c2-e646-11543538c609"
        }

    conn.request("POST", "/rest/vendor/v1/session", payload, headers)

    res = conn.getresponse()
    data = res.read()

    print(data.decode("utf-8"))