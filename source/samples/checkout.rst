
.. code-block:: html

 <html>
 <head><title>Some Solar Purchase</title></head>
 <body>
     <h1>Some Solar</h1>
     <h2>Powerwall Battery: $7,000</h2>
     <p><img src="Powerwall-Battery.jpg"/></p>
     <h3>Buy now with Brighte</h3>
     <h4>[ Include full details and description of item that makes up the price here ]</h4>
     <ul>
         <li>Pay with interest free payment plans.</li>
         <li>Fees apply.</li>
         <li>Repayments of $136.05 per fortnight.</li>
         <li>Total repayable $7,314.48.</li>
     </ul>
     <form action="https://portal.brighte.com.au/checkout?key=YOUR_API_KEY" method="POST">
         <input type="hidden" name="redirect" value="http://yourwebsite.com/checkout-success.html">
         <input type="hidden" name="total_purchase_amount" value="7000">
         <input type="hidden" name="deposit_amount" value="700">
         <input type="hidden" name="repayment_term" value="24">
         <input type="hidden" name="product_category" value="Storage Battery(s)">
         <input type="hidden" name="product_description" value="Powerwall Home Battery">
         <input type="hidden" name="checksum" value="5bf7eef1a95160f47134717996d6c5ae">
         <button type="submit">Buy now with Brighte</button>
     </form>
      <p>*Terms, conditions and lending criteria apply. Repayments based on RRP of $7,000 and 0% deposit.  Minimum amount payable $7,314.48 over 24 months.<br>Fees and charges apply includes $75 Application Fee, $3.50 monthly Account Keeping Fee and $2.99 fortnightly Payment Processing Fee. Ask in-store for details or visit Brighte.com.au. Continuing credit provided by Brighte Ptd Ltd</p>
 </body>
 </html>
