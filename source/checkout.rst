Checkout API
############

Brighte Checkout Application Programming Interface (API) allows the integration of creating loan applications from your server. This gives your business a self-serve experience for your customers to purchase your goods and services with Brighte.


Development
~~~~~~~~~~~

For a quick introduction, `view an example on our demo environment`_ or use the demo API endpoint ``https://demo.brightelabs.com.au/checkout?key=0d5dd2d57ee0fb9973bde47cef60a2ba``. You may also request a fully-functional :doc:`sandbox environment<sandbox>` from Brighte.

.. _view an example on our demo environment: https://demo.brightelabs.com.au/checkout-demo


.. toctree::

   checkout-introduction
   checkout-implementation
