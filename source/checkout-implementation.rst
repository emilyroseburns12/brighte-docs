Implementation
==============

This section details the process of integrating the Checkout API for your business.

Setup
~~~~~

You must first have a vendor account with the Checkout API enabled. Follow these steps:

- Login to the Vendor Portal at ``https://portal.brighte.com.au/login``.
- From the Developer area, click the API keys section.
- Create a new public API key with restrictions for referrer and redirect URLs.

  - The referrer URL restriction specifies the URLs from which API requests are accepted.
  - The redirect URL is a page provided in the API request and to which the customer is redirected after an application. API requests providing invalid redirect URLs will not be accepted.

- You can also create a private key if you want to authenticate the origin of the request.
- Integrate the Checkout API into your web application.

API request
~~~~~~~~~~~

API requests are HTTP requests sent by the customer. They must meet the following conditions:

- The request is sent to ``https://portal.brighte.com.au/checkout?key=YOUR_API_KEY``
- The request must be sent via POST method.
- The request is sent from a URL conforming to the restrictions set on the public API key.
- The redirect URL must conform to the restrictions set on the public API key. This URL can contain a query string component can be used to store parameters between sessions.
- The deposit amount and repayment term must be valid according to your vendor account.
- The product category must be one of the following:

  - Storage Battery(s)
  - Solar Panel Package
  - Solar Panel and Battery Combo
  - Solar Inverter(s)
  - Flooring
  - Guttering
  - Roofing
  - Air-Conditioning
  - Window Shutters
  - Hot Water Systems
  - Pool Heating Systems (Solar/Pump/Gas)
  - Smart Home Technology
  - Trailers and Campers
  - Carports/Patio/Pergolas
  - Doors/Garage Doors
  - Home Improvements
  - Windows/Blinds/Shutters/Glazing
  - Plumbing and Electrical
  - Heating/Cooling Solutions
  - Security System

Required paramters:

 ===================== ========= ============================================
 Parameter             Data type Description
 ===================== ========= ============================================
 redirect              string    Redirect URL upon application completion
 total_purchase_amount double    Purchase total
 deposit_amount        double    Deposit amount
 repayment_term        integer   Repayment term in months
 product_category      string    Product category
 product_description   string    Product description
 reference_number      string    Optional reference number (max 20 chars)
 checksum              string    SHA-256 hash of the POST parameters in the order listed below
 ===================== ========= ============================================

The checksum parameter must be a SHA-256 hash of a concatenated string in this order
  - redirect
  - total_purchase_amount
  - deposit_amount
  - repayment_term
  - product_category
  - product_description

JSON-formatted sample request:

.. code-block:: javascript

    {
      "redirect":"http://yourwebsite.com/checkout-success?yourtrackingid=S888",
      "total_purchase_amount":"7000.12",
      "deposit_amount":"750.24",
      "repayment_term":"24",
      "product_category":"Storage Battery(s)",
      "product_description":"Powerwall Home Battery",
      "reference_number":"12345J",
      "checksum":"5bf7eef1a95160f47134717996d6c5ae"
    }

Sample HTML code that sends an API request:

.. include:: samples/checkout.rst


API response
~~~~~~~~~~~~

If the API request has an invalid ``redirect`` value, a HTTP ``400`` error is returned. If the API request is invalid but has a valid ``redirect`` value, the customer is redirected to the redirect URL with details of the error provided through GET parameters ``error_code`` and ``error_msg``. The error codes are as follows:

 =============== ============================================================
 Error code      Meaning
 =============== ============================================================
 10002           Invalid data given. See ``error_msg`` for more information.
 =============== ============================================================

After the customer submits an application successfully, the customer is redirected to the redirect URL with the following GET values:

 =============== ============================================================
 Parameter       Value
 =============== ============================================================
 error_code      10000
 transaction_id  ID of the application. Unique numeric value.
 =============== ============================================================

Sample HTML and PHP code that receives an API response:

.. include:: samples/checkout-success.rst


Securing webhook
~~~~~~~~~~~~~~~~

As the redirect address is public, you may verify the request came from Brighte using a private API key. If you have created a private API key, extra GET parameters ``signature``, ``timestamp`` and ``token`` are sent. Follow these steps to verify the request:

- Login to the Vendor Portal at ``https://portal.brighte.com.au/login``.
- From the Developer area, Create a new private API key.
- Verify every request by concatenating the ``timestamp`` and ``token`` values and encoding the resulting string with the HMAC algorithm. Use the private API key as the key and SHA256 digest mode. The resulting HEX digest should match the ``signature`` value.

Sample PHP code that verifies an API response:

.. include:: samples/checkout-signature.rst
